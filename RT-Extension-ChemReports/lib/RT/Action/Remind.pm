# BEGIN BPS TAGGED BLOCK {{{
# 
# COPYRIGHT:
# 
# This software is Copyright (c) 1996-2009 Best Practical Solutions, LLC
#                                          <jesse@bestpractical.com>
# 
# (Except where explicitly superseded by other copyright notices)
# 
# 
# LICENSE:
# 
# This work is made available to you under the terms of Version 2 of
# the GNU General Public License. A copy of that license should have
# been provided with this software, but in any event can be snarfed
# from www.gnu.org.
# 
# This work is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 or visit their web page on the internet at
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html.
# 
# 
# CONTRIBUTION SUBMISSION POLICY:
# 
# (The following paragraph is not intended to limit the rights granted
# to you to modify and distribute this software under the terms of
# the GNU General Public License and is only of importance to you if
# you choose to contribute your changes and enhancements to the
# community by submitting them to Best Practical Solutions, LLC.)
# 
# By intentionally submitting any modifications, corrections or
# derivatives to this work, or any other work intended for use with
# Request Tracker, to Best Practical Solutions, LLC, you confirm that
# you are the copyright holder for those contributions and you grant
# Best Practical Solutions,  LLC a nonexclusive, worldwide, irrevocable,
# royalty-free, perpetual, license to use, copy, create derivative
# works based on those contributions, and sublicense and distribute
# those contributions and any derivatives thereof.
# 
# END BPS TAGGED BLOCK }}}

package RT::Action::Remind;
use base 'RT::Action';

use strict;

#Do what we need to do and send it out.

#What does this type of Action does

# {{{ sub Describe 
sub Describe  {
  my $self = shift;
  return (ref $self . " will bump a ticket's reminder status.");
}
# }}}


# {{{ sub Prepare 
sub Prepare  {
    # nothing to prepare
    return 1;
}
# }}}

sub Commit {
    my $self = shift;
    my $firstmsg="
Hi,
 
This is an automated reminder about your ticket concerning 
".$self->TicketObj->Subject.". 
The IT support team believe they are waiting for you to provide some 
information about or an update on the problem. There's no immediate 
hurry for this, but we want to avoid two people thinking that they are 
waiting for each other. 
 
If you would like to see what has been happening on this ticket, you can
see the correspondence and notes at 
https://tickets.ch.cam.ac.uk/rt/SelfService/ .
If the problem is now resolved or no longer an issue, please do send us 
a mail to let us know. If you have a new, unrelated, request, do send a 
new mail to support\@ch.cam.ac.uk to create a new ticket.

If we don't hear back in another ten days, the ticket will be 
automatically closed. It's not a problem to re-open tickets, so if you 
would like us to give some more attention to it even when it's been 
closed, just reply to one of the mails about this ticket number - they 
all have ".$self->TicketObj->EffectiveId." in the subject. 

On behalf of the IT support team,

 Request Tracker
";

    $self->TicketObj->SetStatus($self->Argument);
    my $remcount=0+$self->TicketObj->FirstCustomFieldValue('Reminded');
    my $pester=($self->TicketObj->FirstCustomFieldValue('Pester') eq 'True');
    if ($pester) {return;}
    if ($remcount==0) {
        # Set Reminded to 1
	$self->TicketObj->AddCustomFieldValue(Field=>'Reminded',Value=>1);
        # Send first reminder
	$self->TicketObj->Correspond(Content=>$firstmsg);
	# Set ticket back to "user delay"
	$self->TicketObj->SetStatus("user delay");
    } else {
        # Add comment indicating timeout
	$self->TicketObj->Comment(Content=>"Auto-closing ticket\n");
        # Mark ticket resolved
	$self->TicketObj->SetStatus("resolved");
	# $self->TicketObj->_Set(Field=>'Status',Value=>'Resolved',RecordTransaction=>0);
    }
}

eval "require RT::Action::Remind_Vendor";
die $@ if ($@ && $@ !~ qr{^Can't locate RT/Action/Remind_Vendor.pm});
eval "require RT::Action::Remind_Local";
die $@ if ($@ && $@ !~ qr{^Can't locate RT/Action/Remind_Local.pm});

1;
