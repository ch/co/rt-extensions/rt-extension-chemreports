use v5.8.3;
use strict;
use warnings;

package RT::Extension::ChemReports;

our $VERSION = '0.01';

=pod

=head1 NAME

RT::Extension::ChemReports - Extra reports for Chemistry

=head1 DESCRIPTION

One day I shall write this.

=head1 AUTHOR

Frank Lee E<lt>rl201@cam.ac.ukE<gt>

=head1 LICENSE

This work is made available to you under the terms of Version 2 of
the GNU General Public License. A copy of that license should have
been provided with this software, but in any event can be snarfed
from www.gnu.org.

This work is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 or visit their web page on the internet at
http://www.gnu.org/copyleft/gpl.html.

=cut

1;

